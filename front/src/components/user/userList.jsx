import { useEffect, useState } from "react"
import Api from "../../Api"

export default function User() {
  const [user, setUser] = useState(null)

  useEffect(() => {
    Api.getUser().then(data => {
      console.log(data)
      setUser(data)
    })
  }, [])

  return (
    <>
      {user && user.map((itm, idx) =>
        <div key={idx}>{itm.name}</div>
      )}
    </>
  )
}