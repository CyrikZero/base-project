import axios from "axios"

export default class Api {
  static getUser = async () => {
    const res = await axios.get("/user")
    return res.data
  }
}