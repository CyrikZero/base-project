import { RouterProvider, createBrowserRouter } from 'react-router-dom'
import { useEffect, useState } from 'react';
import Api from './Api';

const router = createBrowserRouter([
  { path: '/user', element: <User /> },
])

function App() {
  return (
    <RouterProvider router={router} />
  );
}

export default App;

function User() {
  const [user, setUser] = useState(null)

  useEffect(() => {
    Api.getUser().then(data => {
      console.log(data)
      setUser(data)
    })
  }, [])

  return (
    <>
      {user && user.map((itm, idx) =>
        <div key={idx}>{itm.name}</div>
      )}
    </>
  )
}