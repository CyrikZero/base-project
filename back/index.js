import Server from "./src/server.js";
import userRoute from "./src/modules/user/user.routes.js"

function startServer() {
  const app = Server();

  app.use("/user", userRoute)
  return app.listen("4000", () => console.log("Server is up! (port : 4000)"))
}

startServer()