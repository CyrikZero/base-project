import db from "../../db.js";

const table = "users"

export default class UserController {
  static getAll = async (req, res) => {
    try {
      const userList = await new Promise((resolve, reject) => {
        db.all(
          `SELECT * FROM ${table}`,
          (err, userList) => err
            ? reject(err)
            : resolve(userList)
        )
      })
      return res.status(200).json(userList)
    } catch (err) {
      return res.status(400).json(err)
    }
  }

  static post = async (req, res) => {
    try {
      const userList = await new Promise((resolve, reject) => {
        db.run(
          `INSERT INTO ${table}(name, email, password) VALUES("test", "email", "psw")`,
          (err, userList) => err
            ? reject(err)
            : resolve(userList)
        )
      })
      return res.status(200).json(userList)
    } catch (err) {
      return res.status(400).json(err)
    }
  }
}