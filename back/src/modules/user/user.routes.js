import { Router } from "express";
import UserController from "./user.controller.js";

const router = Router()

router.get("/", (req, res) => UserController.getAll(req, res))
router.post("/", (req, res) => UserController.post(req, res))

export default router