import sqlite3 from "sqlite3";

const dbUrl = "./database.sqlite3"

const db = new sqlite3.Database(dbUrl, (err) => err
  ? console.log(err)
  : console.log('Connected to db')
)

export default db